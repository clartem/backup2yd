﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace backup2yd
{
	public class DirIsMissingException : Exception
	{
		public DirIsMissingException(string msg) : base(msg)
		{
			
		}
	}

	class SyncClient
	{
		volatile List<SyncTask> tasks;
		volatile Mutex tasksMutex;

		YDAdapter cloudAdapter;

		Thread threadProcess;

		public volatile System.UInt32 SleepInterval;

		public SyncClient(List<SyncTask> _tasks, OauthToken _token, Mutex _tasksMutex)
		{
			tasks = _tasks;
			tasksMutex = _tasksMutex;

			cloudAdapter = new YDAdapter(_token);

			threadProcess = null;

			//Interval in seconds
			SleepInterval = 2;
		}

		public void Launch()
		{
			threadProcess = new Thread(SyncProcess);
			threadProcess.IsBackground = true;
			threadProcess.Start();
		}

		private void SyncProcess()
		{
			List<SyncTask> localTasks = new List<SyncTask>();
			List<SyncTask> badTasks = new List<SyncTask>();

			while (true)
			{
				//Clone list
				tasksMutex.WaitOne();
				localTasks.Clear();
				foreach (SyncTask it in tasks)
				{
					localTasks.Add(it);
				}
				tasksMutex.ReleaseMutex();

				foreach (SyncTask it in localTasks)
				{
					syncDir(it.localPath, it.remotePath);
				}


				Thread.Sleep((int)SleepInterval * 1000);
			}
		}

		private void syncDir(string LocalPath, string RemotePath)
		{
			LocalFSAdapter localFsAdapter = new LocalFSAdapter(LocalPath);

			//Working with files in directory
			DirCollection remoteFiles;
			LocalDirCollection localFiles;
			try
			{
				remoteFiles = cloudAdapter.getFilesOfDir(RemotePath);
				localFiles = localFsAdapter.getFiles();
			}
			catch (DirIsMissingException ex)
			{
				MessageBox.Show(ex.Message);
				return;
			}

			//Deleting files in remote folder that absent in local
			foreach (DirElementInfo it in remoteFiles)
			{
				if (localFiles.Contains(it) == null)
				{
					cloudAdapter.Delete(it.Ref);
				}
			}

			//Uploading files from local folder that newer then same in remote folder or absent there
			foreach (LocalDirElementInfo it in localFiles)
			{
				DirElementInfo match = remoteFiles.Contains(it); 
				bool absentInCloud = (match == null);
				bool localIsNewer = (absentInCloud) ? true : File.GetLastWriteTimeUtc(it.Ref) > Convert.ToDateTime(match.PropStat.Prop.GetLastModified).ToUniversalTime();
				if (absentInCloud || localIsNewer)
				{
					cloudAdapter.UploadFile(it.Ref, RemotePath, it.DisplayName);
				}
			}

			//Working with directoies
			DirCollection remoteSubdirs;
			LocalDirCollection localSubdirs;
			try
			{
				remoteSubdirs = cloudAdapter.getSubdirsOfDir(RemotePath);
				localSubdirs = localFsAdapter.getSubdirs();
			}
			catch (DirIsMissingException ex)
			{
				MessageBox.Show(ex.Message);
				return;
			}

			//Remove all remote folders that absent on machine
			foreach (DirElementInfo it in remoteSubdirs)
			{
				if (localSubdirs.Contains(it) == null)
				{
					cloudAdapter.Delete(it.Ref);
				}
			}

			//Create folders taht absent on server
			foreach (LocalDirElementInfo it in localSubdirs)
			{
				if (remoteSubdirs.Contains(it) == null)
				{
					cloudAdapter.CreateDir(RemotePath, it.DisplayName);
				}
			}

			//Remote list of folders could change on steps above. Check it again
			try
			{
				remoteSubdirs = cloudAdapter.getSubdirsOfDir(RemotePath);
			}
			catch (DirIsMissingException ex)
			{
				MessageBox.Show(ex.Message);
				return;
			}

			//Recursively sync all subdirs
			foreach (LocalDirElementInfo it in localSubdirs)
			{
				DirElementInfo rem = remoteSubdirs.Contains(it);

				//We checked differences between local and remote subdirs, but something could change dirs in machine or server
				if (rem == null)
				{
					MessageBox.Show("Несовпадающие списки удаленных и локальных каталогов. Копия локального каталога " + it.Ref + "отсутствует на сервере");
				}
				else
				{
					syncDir(it.Ref, rem.Ref);
				}
			}
		}

		
	}
}
