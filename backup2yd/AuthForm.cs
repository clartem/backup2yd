﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace backup2yd
{
    public partial class AuthForm : Form
    {
        private OauthToken token;
        public OauthToken authToken
        {
            get { return token; }
        }

        Authorizer auth;

        public AuthForm()
        {
            token = null;
            auth = new Authorizer();
            InitializeComponent();
        }

        private void AuthForm_Load(object sender, EventArgs e)
        {
            auth.getTokenConfirmCode();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                token = auth.getToken(txtConfirmCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            auth.getTokenConfirmCode();
        }
    }
}
