﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace backup2yd
{
	public partial class InputBox : Form
	{
		public InputBox(string Caption, string Message)
		{
			InitializeComponent();

			Text = Caption;
			lblMsg.Text = Message;
		}

		public string Caption
		{
			set { Text = value; }
		}

		public string Message
		{
			set{ lblMsg.Text = value; }
		}

		string userInput;
		public string UserInput
		{
			get { return userInput;	}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			userInput = txtInput.Text;
			Close();
		}

		private void InputBox_Load(object sender, EventArgs e)
		{
			userInput = null;
		}
	}
}
