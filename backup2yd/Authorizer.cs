﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;

using System.Net;
using System.IO;
using System.Web;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace backup2yd
{
    [DataContract]
    public class OauthToken
    {
        [DataMember]
        public string access_token { get; set; }

        [DataMember]
        public string refresh_token { get; set; }

        [DataMember]
        public string token_type { get; set; }

        [DataMember]
        public System.Int64 expires_in { get; set; }
    }

    class Authorizer
    {
        //Address of web page to authorize user
        string AuthAddr;

        //URI parameters
        string _response_type;
        string _client_id;
        string _client_secret;
        string _device_id;

		//Consts
		const string fnameDeviceId = "\\dev_id.info";
		const string fnameToken = "\\token.info";

        public Authorizer()
        {
            //Address of web page to authorize user
            AuthAddr = "https://oauth.yandex.ru/authorize";

            //URI parameters
            _response_type = "response_type=code";
            _client_id = "client_id=916419ea68264b9695ffb959105c5df3";
            _client_secret = "client_secret=6d141d3f06a542cf9a816d0436168398";
            _device_id = "device_id=" + getDeviceID();
        }

        //Methods
        private string getDeviceID()
        {
            String uuid;

            if (System.IO.File.Exists(Environment.CurrentDirectory + fnameDeviceId))
            {
                StreamReader fileReader = null;
                try
                {
                    fileReader = new StreamReader(Environment.CurrentDirectory + fnameDeviceId);
                    uuid = fileReader.ReadToEnd();
                    return uuid;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при чтении идентификатора устройства из файла: " + ex.Message);
                }

                if (fileReader != null)
                {
                    fileReader.Close();
                }
            }

            const int min_len = 6;
            const int max_len = 50;

            const int min_ascii = 65;
            const int max_ascii = 90;

            Random rnd = new Random(DateTime.Now.Millisecond);
            int len = rnd.Next(min_len, max_len);
            Byte[] ascii_bytes = new Byte[len];
            for (int i = 0; i < len; i++)
            {
                ascii_bytes[i] = (Byte)rnd.Next(min_ascii, max_ascii);
            }
            uuid = Encoding.ASCII.GetString(ascii_bytes);

            StreamWriter fileWriter = null;

            try
            {
                FileStream fStream = File.Create(Environment.CurrentDirectory + fnameDeviceId);
				fileWriter = new StreamWriter(fStream);
				fileWriter.Write(uuid);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи идентификатора устройства в файл: " + ex.Message);
            }

            if (fileWriter != null)
            {
                fileWriter.Close();
            }

            return uuid;
        }

        public void getTokenConfirmCode()
        {
            string URI = AuthAddr + "?" + _response_type + "&" + _client_id + "&" + _device_id;
            System.Diagnostics.Process.Start(URI);
        }

        public OauthToken getToken(string confirmCode)
        {
            WebRequest authRequest = WebRequest.Create("https://oauth.yandex.ru/token");
            authRequest.Method = "POST";
            authRequest.ContentType = "application/x-www-form-urlencoded";

            Stream requestBodyStream = authRequest.GetRequestStream();

            String requestBody = "grant_type=authorization_code";
            requestBody += "&code=" + confirmCode;
            requestBody += "&" + _client_id;
            requestBody += "&" + _client_secret;

            Byte[] rb = Encoding.Default.GetBytes(requestBody);
            requestBodyStream.Write(rb, 0, rb.Length);
            requestBodyStream.Close();

            WebResponse authResponse = null;
            try
            {
                authResponse = authRequest.GetResponse();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            Stream responseStream = authResponse.GetResponseStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(OauthToken));

            OauthToken token = (OauthToken)serializer.ReadObject(responseStream);

			FileStream fStream = null;
			try
			{
				fStream = File.Create(Environment.CurrentDirectory + fnameToken);
				serializer.WriteObject(fStream, token);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Невозможно сохранить токен в файл: " + ex.Message);
			}

			if (fStream != null)
			{
				fStream.Close();
			}

            return token;
        }

		public static OauthToken tryReadTokenFromFile()
		{
			OauthToken token = null;
			FileStream fStream = null;

			if (File.Exists(Environment.CurrentDirectory + fnameToken))
			{
				try
				{
					fStream = new FileStream(Environment.CurrentDirectory + fnameToken, FileMode.Open);
					DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(OauthToken));
					token = (OauthToken)serializer.ReadObject(fStream);
				}
				catch(Exception ex)
				{
					MessageBox.Show("Невозможно получить токен из файла: " + ex.Message);
				}
			}

			if (fStream != null)
			{
				fStream.Close();
			}

			return token;
		}
    }
}
