﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.IO;

namespace backup2yd
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
			//All three config files must exist. Otherwise remove all of them
			bool configOk =
			(
				File.Exists(Environment.CurrentDirectory + "\\token.info") &&
				File.Exists(Environment.CurrentDirectory + "\\dev_id.info") &&
				File.Exists(Environment.CurrentDirectory + "\\tasks.info")
			);
			if (!configOk)
			{
				try
				{
					File.Delete(Environment.CurrentDirectory + "\\token.info");
				}
				catch (Exception ex) { } //Ignore... 

				try
				{
					File.Delete(Environment.CurrentDirectory + "\\dev_id.info");
				}
				catch (Exception ex) { } //Ignore... 

				try
				{
					File.Delete(Environment.CurrentDirectory + "\\tasks.info");
				}
				catch (Exception ex) { } //Ignore... 

				
				
			}

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
