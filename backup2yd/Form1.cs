﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace backup2yd
{
	public partial class Form1 : Form
    {
        AuthForm frmAuth;
		AddDirForm frmAddDir;
        OauthToken authToken;

		//List of folders to sync
		volatile List<SyncTask> syncList = new List<SyncTask>();
		volatile System.Threading.Mutex tasksMutex = new System.Threading.Mutex();

		SyncClient DAVClient;

		System.UInt64 taskIdsCounter;

        public Form1()
        {
            frmAuth = new AuthForm();
            authToken = null;
			taskIdsCounter = 0;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
			authToken = Authorizer.tryReadTokenFromFile();

			if (authToken == null)
			{
				frmAuth.ShowDialog();
				authToken = frmAuth.authToken;
			}

			if (authToken == null)
			{
				MessageBox.Show("Не удалось получить доступ к Яндекс.Диску. Приложение будет закрыто");
				Close();
			}

			frmAddDir = new AddDirForm(authToken);

			tblFolders.Columns.Add("localPath", "Локальный каталог");
			tblFolders.Columns.Add("remotePath", "Каталог в облаке");

			loadTasks();

			DAVClient = new SyncClient(syncList, authToken, tasksMutex);
			DAVClient.Launch();
        }

		private void button1_Click(object sender, EventArgs e)
		{
			frmAddDir.ShowDialog();

			if (frmAddDir.IsPathSet)
			{
				//TODO Make it runnable in another thread to make user see no freeze

				tasksMutex.WaitOne();
				syncList.Add(new SyncTask(frmAddDir.LocalPath, frmAddDir.RemotePath, frmAddDir.RemoteDisplayPath, taskIdsCounter++));
				tasksMutex.ReleaseMutex();

				refreshTable();
			}
		}

		private void refreshTable()
		{
			tblFolders.Rows.Clear();
			foreach (SyncTask it in syncList)
			{
				tblFolders.Rows.Add(new Object[] { it.localPath, it.remoteDisplayPath });
				tblFolders.Rows[tblFolders.Rows.Count - 1].Tag = it.Id;
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			//SyncThread is background and does not take any resources,
			//so it is not nessesary to join/cancel thread
			saveTasks();
		}

		static string tasksFileName = "tasks.info";
		private void loadTasks()
		{
			string filePath = Environment.CurrentDirectory + "\\" + tasksFileName;
			syncList.Clear();
			if (File.Exists(filePath))
			{
				FileStream stream = null;
				StreamReader reader = null;
				string line = null;
				int records = 0;

				try
				{
					stream = new FileStream(filePath, FileMode.Open);
					reader = new StreamReader(stream);
					line = reader.ReadLine();
					records = Int32.Parse(line);
				}
				catch (Exception ex)
				{
					MessageBox.Show("Не удалось прочитать число каталогов: " + ex.Message);
					stream.Close();
				}

				string local = "";
				string remote = "";
				string remoteDisplay = "";

				for (int i = 0; i < records; i++)
				{
					try
					{
						local = reader.ReadLine();
						remote = reader.ReadLine();
						remoteDisplay = reader.ReadLine();
					}
					catch (Exception ex)
					{
						MessageBox.Show("Не удалось загрузить список каталогов: " + ex.Message);
						stream.Close();
					}

					syncList.Add(new SyncTask(local, remote, remoteDisplay, taskIdsCounter));
				}

				stream.Close();
			}

			refreshTable();
		}

		private void saveTasks()
		{
			string filePath = Environment.CurrentDirectory + "\\" + tasksFileName;

			FileStream stream = null;
			try
			{
				stream = File.Create(filePath);
				StreamWriter writer = new StreamWriter(stream);

				writer.WriteLine(syncList.Count);

				foreach (SyncTask it in syncList)
				{
					writer.WriteLine(it.localPath);
					writer.WriteLine(it.remotePath);
					writer.WriteLine(it.remoteDisplayPath);
				}
				writer.Flush();

				stream.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Не удалось сохранить списки каталогов: " + ex.Message);
				File.Delete(filePath);
				stream.Close();
				return;
			}
		}

		private void button5_Click(object sender, EventArgs e)
		{
			if (tblFolders.SelectedRows.Count == 0) return;
			ulong taskToDeleteIdx = (ulong) tblFolders.SelectedRows[0].Tag;
			SyncTask toDelete = null;
			foreach (SyncTask it in syncList)
			{
				if (it.Id == taskToDeleteIdx)
				{
					toDelete = it;
					break;
				}
			}

			if (toDelete != null)
			{
				tasksMutex.WaitOne();
				syncList.Remove(toDelete);
				tasksMutex.ReleaseMutex();
			}

			refreshTable();
		}

		private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			switch(WindowState)
			{
				case FormWindowState.Minimized:
					WindowState = FormWindowState.Normal;
					ShowInTaskbar = true;
					Visible = true;
					break;

				default:
					Visible = false;
					ShowInTaskbar = false;
					WindowState = FormWindowState.Minimized;
					break;
			}
		}

    }

	public class SyncTask
	{
		System.UInt64 id;
		public System.UInt64 Id
		{
			get { return id; }
		}

		public string localPath;
		public string remoteDisplayPath;
		public string remotePath;

		public SyncTask(string local, string remote, string remoteDisplay, System.UInt64 _id)
		{
			id = _id;
			localPath = (string) local.Clone();
			remotePath = (string) remote.Clone();
			remoteDisplayPath = (string) remoteDisplay.Clone();
		}
	}
}
