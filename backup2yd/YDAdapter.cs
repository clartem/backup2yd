﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Web;
using System.Net;
using System.Security.Cryptography;

namespace backup2yd
{
	[CollectionDataContract(Name = "multistatus", Namespace = "DAV:", ItemName = "response")]
	public class DirCollection : List<DirElementInfo>
	{
		public DirElementInfo Contains(LocalDirElementInfo element)
		{
			foreach (DirElementInfo it in this)
			{
				if (it.Equals(element)) return it;
			}

			return null;
		}
	}

	[DataContract(Name = "response", Namespace = "DAV:")]
	public class DirElementInfo
	{
		[DataMember(Name = "href", Order = 0)]
		public string Ref { get; set; }

		[DataMember(Name = "propstat", Order = 1)]
		public DirElementPropStat PropStat { get; set; }
	
		public bool IsDir()
		{
			return PropStat.Prop.ContentType == null;
		}

		public bool Equals(LocalDirElementInfo obj)
		{
			return obj.DisplayName == PropStat.Prop.DisplayName;
		}
	}

	[DataContract(Name = "propstat", Namespace = "DAV:")]
	public class DirElementPropStat
	{
		[DataMember(Name = "prop", Order = 1)]
		public DirElementProp Prop { get; set; }
	}

	[DataContract(Name = "prop", Namespace = "DAV:")]
	public class DirElementProp
	{
		[DataMember(Name = "getlastmodified", Order = 0)]
		public string GetLastModified { get; set; }

		[DataMember(Name = "getcontenttype", Order = 1)]
		public string ContentType { get; set; }

		[DataMember(Name = "displayname", Order = 2)]
		public string DisplayName { get; set; }
	}

	public class LocalDirCollection : List<LocalDirElementInfo> 
	{
		public LocalDirElementInfo Contains(DirElementInfo element)
		{
			foreach (LocalDirElementInfo it in this)
			{
				if (it.Equals(element)) return it;
			}

			return null;
		}
	}

	public class LocalDirElementInfo
	{
		public string DisplayName;
		public string Ref;
		
		public bool IsDir()
		{
			return Directory.Exists(Ref);
		}

		public bool Equals(DirElementInfo obj)
		{
			return obj.PropStat.Prop.DisplayName == DisplayName;
		}
	}

	class YDAdapter
	{
		OauthToken token;

		public YDAdapter(OauthToken _token)
		{
			token = _token;
		}

		//path - path to directory from server root
		public DirCollection GetDirectoryContent(string path)
		{
			if (token == null) return null;

			string server_addr = "https://webdav.yandex.ru";
			WebRequest request = WebRequest.Create(server_addr + path);
			try
			{
				request.Method = "PROPFIND";
				request.Headers.Add("Authorization", "OAuth " + token.access_token + "\n");
				request.Headers.Add("Depth", "1\n");
				//request.Headers.Add("Accept", "*/*; ");
			}
			catch (Exception ex)
			{
				MessageBox.Show("Ошибка добавления заголовков в запрос: " + ex.Message);
				return null;
			}

			WebResponse response = null;
			try
			{
				response = request.GetResponse();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Ошибка при получении содержимого каталога на сервере: " + ex.Message);
				return null;
			}


			Stream responseStream = response.GetResponseStream();
			DataContractSerializer serializer = new DataContractSerializer(typeof(DirCollection));
			
			DirCollection coll = null;

			try
			{
				coll = (DirCollection)serializer.ReadObject(responseStream);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return coll;
		}

		public bool UploadFile(string localPath, string DirPath, string fileName)
		{
			const int _16Megs = 16 * 1024 * 1024;
			const int _16Kils = 16 * 1024;

			int StreamBufferSize = _16Megs;

			DirPath = DirPath.TrimEnd('/');
			if (DirPath == "/") DirPath = "";
			
			FileStream fStream;
			int fileLength = 0;
			try
			{
				fStream = new FileStream(localPath, FileMode.Open, FileAccess.Read, FileShare.Read, _16Megs);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return false;
			}

			fileLength = (int) fStream.Length;

			MD5 md5Crypter = MD5.Create();
			byte[] md5Hash = md5Crypter.ComputeHash(fStream);
			String md5HashText = BitConverter.ToString(md5Hash);
			md5HashText = md5HashText.ToLower().Replace("-", "");
			fStream.Close();

			try
			{
				fStream = new FileStream(localPath, FileMode.Open, FileAccess.Read, FileShare.Read, _16Megs);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return false;
			}

			SHA256 sha256Crypter = SHA256.Create();
			byte[] sha256Hash = sha256Crypter.ComputeHash(fStream);
			String sha256HashText = BitConverter.ToString(sha256Hash);
			sha256HashText = sha256HashText.Replace("-", "");
			fStream.Close();

			string request_address = "https://webdav.yandex.ru" + DirPath + "/" + Uri.EscapeDataString(fileName);
			HttpWebRequest request = (HttpWebRequest) WebRequest.Create(request_address);

			request.Accept = "*/*";
			//request.Expect = "100";
			request.ContentType = "application/binary";
			request.ContentLength = fileLength;
			request.Method = "PUT";
			request.Headers.Add("Authorization", "OAuth " + token.access_token + "\n");
			request.Headers.Add("Etag", md5HashText + "\n");
			request.Headers.Add("Sha256", sha256HashText + "\n");

			try
			{
				fStream = new FileStream(localPath, FileMode.Open, FileAccess.Read, FileShare.Read, StreamBufferSize);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			byte[] fileBuffer = new byte[StreamBufferSize];
			int bytesRead = 0;

			request.Proxy = null;
			Stream requestStream = request.GetRequestStream();
			BufferedStream bstream = new BufferedStream(requestStream, StreamBufferSize);

			while ((bytesRead = fStream.Read(fileBuffer, 0, StreamBufferSize)) != 0)
			{
				bstream.Write(fileBuffer, 0, bytesRead);
			}
			
			fStream.Close();			
			bstream.Flush();
			bstream.Close();
			requestStream.Close();
			
			try
			{
				request.GetResponse().Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Не удалось загрузить файл на облако: " + ex.Message);
				return false;
			}

			return true;
		}

		public bool CreateDir(string PathToParentDir, string DirName)
		{
			PathToParentDir = PathToParentDir.TrimEnd('/');

			string requestAddress = "https://webdav.yandex.ru" + PathToParentDir + "/" + Uri.EscapeDataString(DirName) + "/";
			HttpWebRequest request = (HttpWebRequest) WebRequest.Create(requestAddress);

			request.Method = "MKCOL";
			request.Accept = "*/*";
			request.Headers.Add("Authorization", "OAuth " + token.access_token + "\n");

			try
			{
				request.GetResponse();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Не удалось создать каталог на облаке: " + ex.Message);
			}

			return true;
		}

		public bool Delete(string path)
		{
			if (path == "/" || path == "")
			{
				MessageBox.Show("Невозможно удалить корневой каталог");
				return false;
			}

			//path must contain path to file on cloud that recieved from server in <ref></ref>-tag
			string requestAddress = "https://webdav.yandex.ru" + path;
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestAddress);
			request.Method = "DELETE";
			request.Accept = "*/*";
			request.Headers.Add("Authorization", "OAuth " + token.access_token + "\n");

			try
			{
				request.GetResponse();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Не удалось удалить каталог с сервера: " + ex.Message);
				return false;
			}

			return true;
		}

		public DirCollection getFilesOfDir(string path)
		{
			DirCollection allElements = GetDirectoryContent(path);
			DirCollection coll = new DirCollection();

			if (allElements == null) return null;

			foreach (DirElementInfo it in allElements)
			{
				if (!it.IsDir())
				{
					coll.Add(it);
				}
			}

			return coll;
		}

		public DirCollection getSubdirsOfDir(string path)
		{
			path = path.TrimEnd('/');

			DirCollection allElements = GetDirectoryContent(path);
			DirCollection coll = new DirCollection();

			if (allElements == null) return null;

			foreach (DirElementInfo it in allElements)
			{
				if (it.IsDir())
				{
					it.Ref = it.Ref.Substring(0, it.Ref.Length - 1);
					if (it.Ref.TrimEnd('/') != path)
					{
						coll.Add(it);
					}
				}
			}

			return coll;
		}
	
	}
}
