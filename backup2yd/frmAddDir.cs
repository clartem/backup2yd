﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading.Tasks;
using System.IO;

namespace backup2yd
{
	public partial class AddDirForm : Form
	{
		YDAdapter DiskAdapter;

		String localPath;
		public String LocalPath
		{
			get { return localPath; }
		}

		String remotePath;
		public String RemotePath
		{
			get { return remotePath; }
		}

		String remoteDisplayPath;
		public String RemoteDisplayPath
		{
			get { return remoteDisplayPath; }
		}

		bool isPathSet;
		public bool IsPathSet
		{
			get { return isPathSet; }
		}

		public AddDirForm(OauthToken _token)
		{
			InitializeComponent();
			DiskAdapter = new YDAdapter(_token);
		}

		private void frmAddDir_Load(object sender, EventArgs e)
		{
			treeDiskContent.Nodes.Clear();
			treeDiskContent.Nodes.Add("Диск");
			treeDiskContent.Nodes[0].Tag = "/";

			localPath = null;
			remotePath = null;
			isPathSet = false;

			diagFolder.SelectedPath = "";
			lblLocalPath.Text = "Выбрать каталог на компьютере...";

			ShowDiskContent(treeDiskContent.Nodes[0]);
		}

		void ShowDiskContent(TreeNode parentNode)
		{
			if (parentNode == null)
			{
				parentNode = treeDiskContent.Nodes[0];
			}

			treeDiskContent.Enabled = false;
			btnAdd.Enabled = false;
			btnDel.Enabled = false;
			btnOk.Enabled = false;

			lblStatus.Text = "Подождите. Получение данных из облака...";

			parentNode.Nodes.Clear();
			
			if (DiskAdapter != null)
			{
				try
				{
					Task<DirCollection> getDiskContent = new Task<DirCollection>(
						() => DiskAdapter.GetDirectoryContent(
							(string)parentNode.Tag
						)
					);
					getDiskContent.ContinueWith(
						(x) => this.Invoke(
							new putDataToTree_Delegate(putDataToTree), 
							new Object[] {parentNode, x.Result}
						)
					);

					getDiskContent.Start();
				}
				catch (Exception ex)
				{
					MessageBox.Show("Не могу запустить задачу: " + ex.Message);
				}
			}

			
		}

		delegate void putDataToTree_Delegate(TreeNode parentNode, DirCollection coll);
		void putDataToTree(TreeNode parentNode, DirCollection coll)
		{
			if (coll == null)
			{
				treeDiskContent.Enabled = true;
				lblStatus.Text = "При получении данных призошла ошибка";
				return;
			}

			foreach (DirElementInfo it in coll)
			{
				if (it.IsDir())
				{
					if (it.Ref.Trim() == (string)parentNode.Tag) continue;

					try
					{
						parentNode.Nodes.Add(it.PropStat.Prop.DisplayName);
						int idx = parentNode.Nodes.Count;
						parentNode.Nodes[idx - 1].Tag = it.Ref.Trim();
					}
					catch (Exception ex)
					{
						MessageBox.Show("1: " + ex.Message);
					}
				}
			}

			try
			{
				treeDiskContent.Enabled = true;
				btnAdd.Enabled = true;
				btnDel.Enabled = true;
				btnOk.Enabled = true;
				lblStatus.Text = "Готово";
			}
			catch (Exception ex)
			{
				MessageBox.Show("2: " + ex.Message);
			}
		}

		private void treeDiskContent_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			ShowDiskContent(e.Node);
		}

		private void btnOpen_Click(object sender, EventArgs e)
		{
			diagFolder.ShowNewFolderButton = false;
			diagFolder.Description = "Выберите каталог, содержимое которого вы хотите копировать в облако";
			diagFolder.ShowDialog();
			lblLocalPath.Text = diagFolder.SelectedPath;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			if (!Directory.Exists(diagFolder.SelectedPath))
			{
				MessageBox.Show("Не выбран каталог, который нужно копировать в облако");
				return;
			}

			if (treeDiskContent.SelectedNode == null)
			{
				MessageBox.Show("Не выбран каталог в облачном хранилище");
				return;
			}

			localPath = diagFolder.SelectedPath;
			remotePath = (String)treeDiskContent.SelectedNode.Tag;
			remoteDisplayPath = getDisplayPath(treeDiskContent.SelectedNode);
			isPathSet = true;
			Close();
		}

		private string getDisplayPath(TreeNode node)
		{
			if (node.Level == 0)
			{
				return "/";
			}

			string path = node.Text;
			for (TreeNode it = node.Parent; it.Level != 0; it = it.Parent)
			{
				path = it.Text + "/" + path;
			}
			path = "/" + path;

			return path;
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			if (treeDiskContent.SelectedNode == null)
			{
				MessageBox.Show("Выберите каталог, в котором создать новую папку");
				return;
			}

			InputBox frmInput = new InputBox("Ввод", "Введите имя нового каталога");
			frmInput.ShowDialog();
			if (frmInput.UserInput != null)
			{
				DiskAdapter.CreateDir
				(
					(string) treeDiskContent.SelectedNode.Tag, 
					frmInput.UserInput
				);
				ShowDiskContent(treeDiskContent.SelectedNode);
			}
		}



		private void treeDiskContent_AfterSelect(object sender, TreeViewEventArgs e)
		{

		}

		private void btnDel_Click(object sender, EventArgs e)
		{
			if (treeDiskContent.SelectedNode == null)
			{
				MessageBox.Show("Выберите каталог для удаления");
			}

			treeDiskContent.Enabled = false;
			btnDel.Enabled = false;
			btnOk.Enabled = false;
			btnAdd.Enabled = false;
			lblStatus.Text = "Подождите. Получение данных из облака...";

			//DirCollection coll = DiskAdapter.GetDirectoryContent((string)treeDiskContent.SelectedNode.Tag);
			try
			{
				String pathToDel = (string)treeDiskContent.SelectedNode.Tag;
				TreeNode nodeToRefresh = treeDiskContent.SelectedNode;

				Task<DirCollection> delTask = new Task<DirCollection>
				(
					() => DiskAdapter.GetDirectoryContent(
						pathToDel
					)
				);
				delTask.ContinueWith
				(
					(x) => deleteConfirm(x.Result, nodeToRefresh)
				);
				delTask.Start();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		delegate void show_dc_delegate(TreeNode node);
		void deleteConfirm(DirCollection coll, TreeNode node)
		{
			if (coll == null)
			{
				Invoke(new show_dc_delegate(ShowDiskContent), new object[] { treeDiskContent.SelectedNode });
				return;
			}

			//PROPFIND returns catalog itself with its content, so empty collection always has size = 1
			if (coll.Count > 1)
			{
				DialogResult res = MessageBox.Show("Выбранный каталог не пуст, вы точно хотите его удалить?\nВНИМАНИЕ: Каталог будет удален вместе со всем содержимым", "Подтвердите", MessageBoxButtons.OKCancel);
				if (res == System.Windows.Forms.DialogResult.OK)
				{
					deleteChosenDir(node);
				}
			}
			else
			{
				deleteChosenDir(node);
			}
		}

		void deleteChosenDir(TreeNode node)
		{
			try
			{
				Task<bool> delete = new Task<bool>(() => DiskAdapter.Delete((string)node.Tag));
				delete.ContinueWith
				(
					(x) => Invoke(
						new show_dc_delegate(ShowDiskContent),
						new object[] {node.Parent}
					)
				);
				delete.Start();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}


	}
}
