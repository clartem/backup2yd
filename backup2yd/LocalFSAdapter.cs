﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Windows.Forms;

namespace backup2yd
{
	class LocalFSAdapter
	{
		string DirPath;

		public LocalFSAdapter(string path)
		{
			DirPath = path;
		}

		static char pathSeparator = '\\';
		public LocalDirCollection getFiles()
		{
			LocalDirCollection coll = new LocalDirCollection();

			string[] fileNames = null;
			try
			{
				fileNames = Directory.GetFiles(DirPath);
			}
			catch (DirectoryNotFoundException ex)
			{
				throw new DirIsMissingException("Не удалось получить списко файлов каталога " + DirPath + "\nВероятно, он был удален пользователем после того, как началась его синхронизация: " + ex.Message);
			}

			foreach (string it in fileNames)
			{
				LocalDirElementInfo file = new LocalDirElementInfo();
				file.Ref = it;
				file.DisplayName = it.Substring(it.LastIndexOf(pathSeparator) + 1);
				coll.Add(file);
			}

			return coll;
		}

		public LocalDirCollection getSubdirs()
		{
			LocalDirCollection coll = new LocalDirCollection();

			string[] fileNames;

			try
			{
				fileNames = Directory.GetDirectories(DirPath);
			}
			catch (DirectoryNotFoundException ex)
			{
				throw new DirIsMissingException("Не удалось получить списко подкаталогов " + DirPath + "\nВероятно, он был удален пользователем после того, как началась его синхронизация: " + ex.Message);
			}

			foreach (string it in fileNames)
			{
				LocalDirElementInfo file = new LocalDirElementInfo();
				file.Ref = it;
				file.DisplayName = it.Substring(it.LastIndexOf(pathSeparator) + 1);
				coll.Add(file);
			}

			return coll;
		}

	}
}
